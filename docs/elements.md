# Elements

As the word says, the Elements are the simple constructs you can use to build your site.
Below you see the generated HTML for each of the elements you add to your site. Understanding
this HTML will help in styling your site.

Every of the elements have configurable properties that influence. These properties can usually
be set in the floating dialog of the currently selected element.

All the generated HTML is done within the body container, but for every section you add, there will
be an additional `section` container. A section element always has a nested `div` with class `zpcontainer`.

```html
<!-- === page.content BEGIN -->
<div class="zpcontent-container page-container ">
    <div data-element-id="elm_R4mNWQEsCLwCLarLmI6Y9w" data-element-type="section" class="zpsection " data-background-type="">
        <div class="zpcontainer">
        <!-- html for element here -->
        </div>
    </div>
    <!-- more sections here -->
</div>
<!-- === page.content END -->
```

Below you will find the generated HTML for each of the elements you can add to your page in Zoho sites.
However, when you test adding single elements to an empty page or section, you will find these elements
being wrapped within a single column [`row`](#row) element.

```html
<div data-element-id="elm_J4BAQDVi1S4iJKtQwVaYtQ" data-element-type="row" class="zprow zpalign-items- zpjustify-content-"
    data-column-ratio="12,12">
    <div data-element-id="elm_HmzIpyhnZGQyvKFLDXRiTA" data-element-type="column" class="zpcol-md-12 zpcol-sm-12 zpdefault-section zpdefault-section-bg "
        data-background-type="none">
        <!-- HTML of element here -->
    </div>
</div>
```

### <a name="gallery">Gallery

```html
<div data-element-id="elm_I264abnJTAM3dpOkuBOHUw" data-element-type="gallery" class="zpelement zpelem-gallery "
    itemscope="">
    <div class="zpgallery-container hb-layout__cont" data-photoset_id="1029778000000071081" data-gallery_type="1">
        <div class="hb-grid-gallery hb-lightbox hb-layout no-fill-with-last " data-album_name="samples"
            data-columns="5" data-thumbs="true" data-hover_animation="zoomin" data-captions="false" data-image_background=""
            data-caption_animation="slideup" itemscope itemtype="http://schema.org/ImageGallery" data-grid__gutter="1"
            data-layout-type="square" data-lightbox-options="thumbs: true,caption:false,type:fullscreen,theme:dark">
            <div class="hb-grid-item">
                <figure itemscope itemtype="http://schema.org/ImageObject">
                    <a href="javascript:;">
                        <img data-src="/photoset/samples/IMG_4765.jpg" src="/photoset/samples/.IMG_4765.jpg_m.jpg"
                        />
                        <figcaption class="hb-grid-caption zpimage-caption">
                            <h4>Title</h4>
                            <p>Caption</p>
                        </figcaption>
                    </a>
                </figure>
            </div>
            <div class="hb-grid-item">
                <figure itemscope itemtype="http://schema.org/ImageObject">
                    <a href="javascript:;">
                        <img data-src="/photoset/samples/IMG_4766.jpg" src="/photoset/samples/.IMG_4766.jpg_m.jpg"
                        />
                        <figcaption class="hb-grid-caption zpimage-caption">
                            <h4>Title</h4>
                            <p>Caption</p>
                        </figcaption>
                    </a>
                </figure>
            </div>
        </div>
    </div>
</div>
```

## Content

### <a name="tabs">Tabs

Three tabs, without further content.

```html
<div data-element-id="elm_nbld-4-HBfYPxm7SnnqDtg" data-element-type="tabs" class="zpelement zpelem-tabs "
    data-icon="true">
    <div class="zptabelem-inner-container zptabs-style-01 zptab-type-01 zptabs-align-center zptabicon-align-left zptabicon-size-sm ">
        <div class="zptabs-container">
            <div data-element-id="elm_V2zSL6pnpArKT5_PICYrLQ" data-element-type="tabheader" data-tab-name="TAB 1"
                data-content-id="elm_sVwqcgq-5qhwWRQmDcgJog" class="zpelement zptab " style="margin-top:0;">
                <span class="zptabicon">
                    <svg viewBox="0 0 256 237" xmlns="http://www.w3.org/2000/svg">
                        <path d="M128,0.289999962 L134.690909,6.68999996 L255.709091,127.708182 L242.327273,141.09 L230.4,129.162727 L230.4,227.49 L230.4,236.799091 L221.090909,236.799091 L155.927273,236.799091 L146.618182,236.799091 L146.618182,227.49 L146.618182,143.708182 L109.381818,143.708182 L109.381818,227.49 L109.381818,236.799091 L100.072727,236.799091 L34.9090909,236.799091 L25.6,236.799091 L25.6,227.49 L25.6,129.162727 L13.6727273,141.09 L0.290909091,127.708182 L121.309091,6.68999996 L128,0.289999962 Z M128,26.7627272 L44.2181818,110.544545 L44.2181818,218.180909 L90.7636364,218.180909 L90.7636364,134.399091 L90.7636364,125.09 L100.072727,125.09 L155.927273,125.09 L165.236364,125.09 L165.236364,134.399091 L165.236364,218.180909 L211.781818,218.180909 L211.781818,110.544545 L128,26.7627272 Z"
                            fill-rule="evenodd"></path>
                    </svg>
                </span>
                <div>
                    <span class="zptab-name">TAB 1</span>
                </div>
            </div>
            <div data-element-id="elm_C1TKBqH7onXq2VCtO5pBLw" data-element-type="tabheader" data-tab-name="TAB 2"
                data-content-id="elm_aJFotNGV_AtGEdvv7MpIXA" class="zpelement zptab " style="margin-top:0;">
                <span class="zptabicon">
                    <svg viewBox="0 0 256 237" xmlns="http://www.w3.org/2000/svg">
                        <path d="M128,0.289999962 L134.690909,6.68999996 L255.709091,127.708182 L242.327273,141.09 L230.4,129.162727 L230.4,227.49 L230.4,236.799091 L221.090909,236.799091 L155.927273,236.799091 L146.618182,236.799091 L146.618182,227.49 L146.618182,143.708182 L109.381818,143.708182 L109.381818,227.49 L109.381818,236.799091 L100.072727,236.799091 L34.9090909,236.799091 L25.6,236.799091 L25.6,227.49 L25.6,129.162727 L13.6727273,141.09 L0.290909091,127.708182 L121.309091,6.68999996 L128,0.289999962 Z M128,26.7627272 L44.2181818,110.544545 L44.2181818,218.180909 L90.7636364,218.180909 L90.7636364,134.399091 L90.7636364,125.09 L100.072727,125.09 L155.927273,125.09 L165.236364,125.09 L165.236364,134.399091 L165.236364,218.180909 L211.781818,218.180909 L211.781818,110.544545 L128,26.7627272 Z"
                            fill-rule="evenodd"></path>
                    </svg>
                </span>
                <div>
                    <span class="zptab-name">TAB 2</span>
                </div>
            </div>
            <div data-element-id="elm_Xm8PuGeEbBu0q1RjVsGimA" data-element-type="tabheader" data-tab-name="TAB 3"
                data-content-id="elm_2HYKTllIeCftdGZn-zBA3A" class="zpelement zptab " style="margin-top:0;">
                <span class="zptabicon">
                    <svg viewBox="0 0 256 237" xmlns="http://www.w3.org/2000/svg">
                        <path d="M128,0.289999962 L134.690909,6.68999996 L255.709091,127.708182 L242.327273,141.09 L230.4,129.162727 L230.4,227.49 L230.4,236.799091 L221.090909,236.799091 L155.927273,236.799091 L146.618182,236.799091 L146.618182,227.49 L146.618182,143.708182 L109.381818,143.708182 L109.381818,227.49 L109.381818,236.799091 L100.072727,236.799091 L34.9090909,236.799091 L25.6,236.799091 L25.6,227.49 L25.6,129.162727 L13.6727273,141.09 L0.290909091,127.708182 L121.309091,6.68999996 L128,0.289999962 Z M128,26.7627272 L44.2181818,110.544545 L44.2181818,218.180909 L90.7636364,218.180909 L90.7636364,134.399091 L90.7636364,125.09 L100.072727,125.09 L155.927273,125.09 L165.236364,125.09 L165.236364,134.399091 L165.236364,218.180909 L211.781818,218.180909 L211.781818,110.544545 L128,26.7627272 Z"
                            fill-rule="evenodd"></path>
                    </svg>
                </span>
                <div>
                    <span class="zptab-name">TAB 3</span>
                </div>
            </div>
        </div>
        <div class="zptabs-content-container">
            <div data-element-id="elm_V2zSL6pnpArKT5_PICYrLQ" data-element-type="tabheader" data-tab-name="TAB 1"
                data-content-id="elm_sVwqcgq-5qhwWRQmDcgJog" class="zpelement zptab " style="margin-top:0;">
                <span class="zptabicon">
                    <svg viewBox="0 0 256 237" xmlns="http://www.w3.org/2000/svg">
                        <path d="M128,0.289999962 L134.690909,6.68999996 L255.709091,127.708182 L242.327273,141.09 L230.4,129.162727 L230.4,227.49 L230.4,236.799091 L221.090909,236.799091 L155.927273,236.799091 L146.618182,236.799091 L146.618182,227.49 L146.618182,143.708182 L109.381818,143.708182 L109.381818,227.49 L109.381818,236.799091 L100.072727,236.799091 L34.9090909,236.799091 L25.6,236.799091 L25.6,227.49 L25.6,129.162727 L13.6727273,141.09 L0.290909091,127.708182 L121.309091,6.68999996 L128,0.289999962 Z M128,26.7627272 L44.2181818,110.544545 L44.2181818,218.180909 L90.7636364,218.180909 L90.7636364,134.399091 L90.7636364,125.09 L100.072727,125.09 L155.927273,125.09 L165.236364,125.09 L165.236364,134.399091 L165.236364,218.180909 L211.781818,218.180909 L211.781818,110.544545 L128,26.7627272 Z"
                            fill-rule="evenodd"></path>
                    </svg>
                </span>
                <div>
                    <span class="zptab-name">TAB 1</span>
                </div>
            </div>
            <div data-element-id="elm_sVwqcgq-5qhwWRQmDcgJog" data-element-type="tabcontainer" data-header-id="elm_V2zSL6pnpArKT5_PICYrLQ"
                class="zpelement zptab-content " style="margin-top:0;">
                <div class="zptab-element-container"></div>
            </div>
            <div data-element-id="elm_C1TKBqH7onXq2VCtO5pBLw" data-element-type="tabheader" data-tab-name="TAB 2"
                data-content-id="elm_aJFotNGV_AtGEdvv7MpIXA" class="zpelement zptab " style="margin-top:0;">
                <span class="zptabicon">
                    <svg viewBox="0 0 256 237" xmlns="http://www.w3.org/2000/svg">
                        <path d="M128,0.289999962 L134.690909,6.68999996 L255.709091,127.708182 L242.327273,141.09 L230.4,129.162727 L230.4,227.49 L230.4,236.799091 L221.090909,236.799091 L155.927273,236.799091 L146.618182,236.799091 L146.618182,227.49 L146.618182,143.708182 L109.381818,143.708182 L109.381818,227.49 L109.381818,236.799091 L100.072727,236.799091 L34.9090909,236.799091 L25.6,236.799091 L25.6,227.49 L25.6,129.162727 L13.6727273,141.09 L0.290909091,127.708182 L121.309091,6.68999996 L128,0.289999962 Z M128,26.7627272 L44.2181818,110.544545 L44.2181818,218.180909 L90.7636364,218.180909 L90.7636364,134.399091 L90.7636364,125.09 L100.072727,125.09 L155.927273,125.09 L165.236364,125.09 L165.236364,134.399091 L165.236364,218.180909 L211.781818,218.180909 L211.781818,110.544545 L128,26.7627272 Z"
                            fill-rule="evenodd"></path>
                    </svg>
                </span>
                <div>
                    <span class="zptab-name">TAB 2</span>
                </div>
            </div>
            <div data-element-id="elm_aJFotNGV_AtGEdvv7MpIXA" data-element-type="tabcontainer" data-header-id="elm_C1TKBqH7onXq2VCtO5pBLw"
                class="zpelement zptab-content " style="margin-top:0;">
                <div class="zptab-element-container"></div>
            </div>
            <div data-element-id="elm_Xm8PuGeEbBu0q1RjVsGimA" data-element-type="tabheader" data-tab-name="TAB 3"
                data-content-id="elm_2HYKTllIeCftdGZn-zBA3A" class="zpelement zptab " style="margin-top:0;">
                <span class="zptabicon">
                    <svg viewBox="0 0 256 237" xmlns="http://www.w3.org/2000/svg">
                        <path d="M128,0.289999962 L134.690909,6.68999996 L255.709091,127.708182 L242.327273,141.09 L230.4,129.162727 L230.4,227.49 L230.4,236.799091 L221.090909,236.799091 L155.927273,236.799091 L146.618182,236.799091 L146.618182,227.49 L146.618182,143.708182 L109.381818,143.708182 L109.381818,227.49 L109.381818,236.799091 L100.072727,236.799091 L34.9090909,236.799091 L25.6,236.799091 L25.6,227.49 L25.6,129.162727 L13.6727273,141.09 L0.290909091,127.708182 L121.309091,6.68999996 L128,0.289999962 Z M128,26.7627272 L44.2181818,110.544545 L44.2181818,218.180909 L90.7636364,218.180909 L90.7636364,134.399091 L90.7636364,125.09 L100.072727,125.09 L155.927273,125.09 L165.236364,125.09 L165.236364,134.399091 L165.236364,218.180909 L211.781818,218.180909 L211.781818,110.544545 L128,26.7627272 Z"
                            fill-rule="evenodd"></path>
                    </svg>
                </span>
                <div>
                    <span class="zptab-name">TAB 3</span>
                </div>
            </div>
            <div data-element-id="elm_2HYKTllIeCftdGZn-zBA3A" data-element-type="tabcontainer" data-header-id="elm_Xm8PuGeEbBu0q1RjVsGimA"
                class="zpelement zptab-content " style="margin-top:0;">
                <div class="zptab-element-container"></div>
            </div>
        </div>
    </div>
</div>
```

### <a name="accordion">Accordion

Three tabs without any further content

```html
<div data-element-id="elm_0psF5x83Hp0oHTgtXWxJEg" data-element-type="accordion" class="zpelement zpelem-accordion "
    data-icon="true" data-icon-style="1">
    <div class="zpaccordion-container zpaccordion-style-01 zpaccordion-with-icon zpaccord-svg-icon-1 zpaccordion-icon-align-left ">
        <div data-element-id="elm_HdrKKgt63YFco5sXNnR68Q" data-element-type="accordionheader" class="zpelement zpaccordion "
            data-tab-name="TAB 1" data-content-id="elm_-ls8Rd2a5Umbch19ZmR5CA" style="margin-top:0;">
            <span class="zpaccordion-name">TAB 1</span>
            <span class="zpaccordionicon zpaccord-icon-inactive">
                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-1">
                    <path d="M98.9,184.7l1.8,2.1l136,156.5c4.6,5.3,11.5,8.6,19.2,8.6c7.7,0,14.6-3.4,19.2-8.6L411,187.1l2.3-2.6 c1.7-2.5,2.7-5.5,2.7-8.7c0-8.7-7.4-15.8-16.6-15.8v0H112.6v0c-9.2,0-16.6,7.1-16.6,15.8C96,179.1,97.1,182.2,98.9,184.7z"></path>
                </svg>
                <svg viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-2">
                    <path d="M128,169.174c-1.637,0-3.276-0.625-4.525-1.875l-56.747-56.747c-2.5-2.499-2.5-6.552,0-9.05c2.497-2.5,6.553-2.5,9.05,0 L128,153.722l52.223-52.22c2.496-2.5,6.553-2.5,9.049,0c2.5,2.499,2.5,6.552,0,9.05l-56.746,56.747 C131.277,168.549,129.638,169.174,128,169.174z M256,128C256,57.42,198.58,0,128,0C57.42,0,0,57.42,0,128c0,70.58,57.42,128,128,128 C198.58,256,256,198.58,256,128z M243.2,128c0,63.521-51.679,115.2-115.2,115.2c-63.522,0-115.2-51.679-115.2-115.2 C12.8,64.478,64.478,12.8,128,12.8C191.521,12.8,243.2,64.478,243.2,128z"></path>
                </svg>
                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-3">
                    <path d="M256,298.3L256,298.3L256,298.3l174.2-167.2c4.3-4.2,11.4-4.1,15.8,0.2l30.6,29.9c4.4,4.3,4.5,11.3,0.2,15.5L264.1,380.9c-2.2,2.2-5.2,3.2-8.1,3c-3,0.1-5.9-0.9-8.1-3L35.2,176.7c-4.3-4.2-4.2-11.2,0.2-15.5L66,131.3c4.4-4.3,11.5-4.4,15.8-0.2L256,298.3z"
                    />
                </svg>
                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-4">
                    <path d="M417.4,224H288V94.6c0-16.9-14.3-30.6-32-30.6c-17.7,0-32,13.7-32,30.6V224H94.6C77.7,224,64,238.3,64,256 c0,17.7,13.7,32,30.6,32H224v129.4c0,16.9,14.3,30.6,32,30.6c17.7,0,32-13.7,32-30.6V288h129.4c16.9,0,30.6-14.3,30.6-32 C448,238.3,434.3,224,417.4,224z"></path>
                </svg>
            </span>
            <span class="zpaccordionicon zpaccord-icon-active">
                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-1">
                    <path d="M413.1,327.3l-1.8-2.1l-136-156.5c-4.6-5.3-11.5-8.6-19.2-8.6c-7.7,0-14.6,3.4-19.2,8.6L101,324.9l-2.3,2.6 C97,330,96,333,96,336.2c0,8.7,7.4,15.8,16.6,15.8v0h286.8v0c9.2,0,16.6-7.1,16.6-15.8C416,332.9,414.9,329.8,413.1,327.3z"></path>
                </svg>
                <svg viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-2">
                    <path d="M184.746,156.373c-1.639,0-3.275-0.625-4.525-1.875L128,102.278l-52.223,52.22c-2.497,2.5-6.55,2.5-9.05,0 c-2.5-2.498-2.5-6.551,0-9.05l56.749-56.747c1.2-1.2,2.828-1.875,4.525-1.875l0,0c1.697,0,3.325,0.675,4.525,1.875l56.745,56.747 c2.5,2.499,2.5,6.552,0,9.05C188.021,155.748,186.383,156.373,184.746,156.373z M256,128C256,57.42,198.58,0,128,0 C57.42,0,0,57.42,0,128c0,70.58,57.42,128,128,128C198.58,256,256,198.58,256,128z M243.2,128c0,63.521-51.679,115.2-115.2,115.2 c-63.522,0-115.2-51.679-115.2-115.2C12.8,64.478,64.478,12.8,128,12.8C191.521,12.8,243.2,64.478,243.2,128z"></path>
                </svg>
                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-3">
                    <path d="M256,213.7L256,213.7L256,213.7l174.2,167.2c4.3,4.2,11.4,4.1,15.8-0.2l30.6-29.9c4.4-4.3,4.5-11.3,0.2-15.5L264.1,131.1c-2.2-2.2-5.2-3.2-8.1-3c-3-0.1-5.9,0.9-8.1,3L35.2,335.3c-4.3,4.2-4.2,11.2,0.2,15.5L66,380.7c4.4,4.3,11.5,4.4,15.8,0.2L256,213.7z"
                    />
                </svg>
                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-4">
                    <path d="M417.4,224H94.6C77.7,224,64,238.3,64,256c0,17.7,13.7,32,30.6,32h322.8c16.9,0,30.6-14.3,30.6-32 C448,238.3,434.3,224,417.4,224z"></path>
                </svg>
            </span>
        </div>
        <div data-element-id="elm_-ls8Rd2a5Umbch19ZmR5CA" data-element-type="accordioncontainer" class="zpelement zpaccordion-content "
            style="margin-top:0;">
            <div class="zpaccordion-element-container"></div>
        </div>
        <div data-element-id="elm_p-TCalOCcr4s4G7mcBI9ag" data-element-type="accordionheader" class="zpelement zpaccordion "
            data-tab-name="TAB 2" data-content-id="elm_ZDBsYymAs8BqYFf8epzEAA" style="margin-top:0;">
            <span class="zpaccordion-name">TAB 2</span>
            <span class="zpaccordionicon zpaccord-icon-inactive">
                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-1">
                    <path d="M98.9,184.7l1.8,2.1l136,156.5c4.6,5.3,11.5,8.6,19.2,8.6c7.7,0,14.6-3.4,19.2-8.6L411,187.1l2.3-2.6 c1.7-2.5,2.7-5.5,2.7-8.7c0-8.7-7.4-15.8-16.6-15.8v0H112.6v0c-9.2,0-16.6,7.1-16.6,15.8C96,179.1,97.1,182.2,98.9,184.7z"></path>
                </svg>
                <svg viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-2">
                    <path d="M128,169.174c-1.637,0-3.276-0.625-4.525-1.875l-56.747-56.747c-2.5-2.499-2.5-6.552,0-9.05c2.497-2.5,6.553-2.5,9.05,0 L128,153.722l52.223-52.22c2.496-2.5,6.553-2.5,9.049,0c2.5,2.499,2.5,6.552,0,9.05l-56.746,56.747 C131.277,168.549,129.638,169.174,128,169.174z M256,128C256,57.42,198.58,0,128,0C57.42,0,0,57.42,0,128c0,70.58,57.42,128,128,128 C198.58,256,256,198.58,256,128z M243.2,128c0,63.521-51.679,115.2-115.2,115.2c-63.522,0-115.2-51.679-115.2-115.2 C12.8,64.478,64.478,12.8,128,12.8C191.521,12.8,243.2,64.478,243.2,128z"></path>
                </svg>
                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-3">
                    <path d="M256,298.3L256,298.3L256,298.3l174.2-167.2c4.3-4.2,11.4-4.1,15.8,0.2l30.6,29.9c4.4,4.3,4.5,11.3,0.2,15.5L264.1,380.9c-2.2,2.2-5.2,3.2-8.1,3c-3,0.1-5.9-0.9-8.1-3L35.2,176.7c-4.3-4.2-4.2-11.2,0.2-15.5L66,131.3c4.4-4.3,11.5-4.4,15.8-0.2L256,298.3z"
                    />
                </svg>
                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-4">
                    <path d="M417.4,224H288V94.6c0-16.9-14.3-30.6-32-30.6c-17.7,0-32,13.7-32,30.6V224H94.6C77.7,224,64,238.3,64,256 c0,17.7,13.7,32,30.6,32H224v129.4c0,16.9,14.3,30.6,32,30.6c17.7,0,32-13.7,32-30.6V288h129.4c16.9,0,30.6-14.3,30.6-32 C448,238.3,434.3,224,417.4,224z"></path>
                </svg>
            </span>
            <span class="zpaccordionicon zpaccord-icon-active">
                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-1">
                    <path d="M413.1,327.3l-1.8-2.1l-136-156.5c-4.6-5.3-11.5-8.6-19.2-8.6c-7.7,0-14.6,3.4-19.2,8.6L101,324.9l-2.3,2.6 C97,330,96,333,96,336.2c0,8.7,7.4,15.8,16.6,15.8v0h286.8v0c9.2,0,16.6-7.1,16.6-15.8C416,332.9,414.9,329.8,413.1,327.3z"></path>
                </svg>
                <svg viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-2">
                    <path d="M184.746,156.373c-1.639,0-3.275-0.625-4.525-1.875L128,102.278l-52.223,52.22c-2.497,2.5-6.55,2.5-9.05,0 c-2.5-2.498-2.5-6.551,0-9.05l56.749-56.747c1.2-1.2,2.828-1.875,4.525-1.875l0,0c1.697,0,3.325,0.675,4.525,1.875l56.745,56.747 c2.5,2.499,2.5,6.552,0,9.05C188.021,155.748,186.383,156.373,184.746,156.373z M256,128C256,57.42,198.58,0,128,0 C57.42,0,0,57.42,0,128c0,70.58,57.42,128,128,128C198.58,256,256,198.58,256,128z M243.2,128c0,63.521-51.679,115.2-115.2,115.2 c-63.522,0-115.2-51.679-115.2-115.2C12.8,64.478,64.478,12.8,128,12.8C191.521,12.8,243.2,64.478,243.2,128z"></path>
                </svg>
                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-3">
                    <path d="M256,213.7L256,213.7L256,213.7l174.2,167.2c4.3,4.2,11.4,4.1,15.8-0.2l30.6-29.9c4.4-4.3,4.5-11.3,0.2-15.5L264.1,131.1c-2.2-2.2-5.2-3.2-8.1-3c-3-0.1-5.9,0.9-8.1,3L35.2,335.3c-4.3,4.2-4.2,11.2,0.2,15.5L66,380.7c4.4,4.3,11.5,4.4,15.8,0.2L256,213.7z"
                    />
                </svg>
                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-4">
                    <path d="M417.4,224H94.6C77.7,224,64,238.3,64,256c0,17.7,13.7,32,30.6,32h322.8c16.9,0,30.6-14.3,30.6-32 C448,238.3,434.3,224,417.4,224z"></path>
                </svg>
            </span>
        </div>
        <div data-element-id="elm_ZDBsYymAs8BqYFf8epzEAA" data-element-type="accordioncontainer" class="zpelement zpaccordion-content "
            style="margin-top:0;">
            <div class="zpaccordion-element-container"></div>
        </div>
        <div data-element-id="elm_hkxYkAYIUJ-u90fwJ4fo4g" data-element-type="accordionheader" class="zpelement zpaccordion "
            data-tab-name="TAB 3" data-content-id="elm_aPCWrLzWSj71lfwF5_Zi9A" style="margin-top:0;">
            <span class="zpaccordion-name">TAB 3</span>
            <span class="zpaccordionicon zpaccord-icon-inactive">
                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-1">
                    <path d="M98.9,184.7l1.8,2.1l136,156.5c4.6,5.3,11.5,8.6,19.2,8.6c7.7,0,14.6-3.4,19.2-8.6L411,187.1l2.3-2.6 c1.7-2.5,2.7-5.5,2.7-8.7c0-8.7-7.4-15.8-16.6-15.8v0H112.6v0c-9.2,0-16.6,7.1-16.6,15.8C96,179.1,97.1,182.2,98.9,184.7z"></path>
                </svg>
                <svg viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-2">
                    <path d="M128,169.174c-1.637,0-3.276-0.625-4.525-1.875l-56.747-56.747c-2.5-2.499-2.5-6.552,0-9.05c2.497-2.5,6.553-2.5,9.05,0 L128,153.722l52.223-52.22c2.496-2.5,6.553-2.5,9.049,0c2.5,2.499,2.5,6.552,0,9.05l-56.746,56.747 C131.277,168.549,129.638,169.174,128,169.174z M256,128C256,57.42,198.58,0,128,0C57.42,0,0,57.42,0,128c0,70.58,57.42,128,128,128 C198.58,256,256,198.58,256,128z M243.2,128c0,63.521-51.679,115.2-115.2,115.2c-63.522,0-115.2-51.679-115.2-115.2 C12.8,64.478,64.478,12.8,128,12.8C191.521,12.8,243.2,64.478,243.2,128z"></path>
                </svg>
                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-3">
                    <path d="M256,298.3L256,298.3L256,298.3l174.2-167.2c4.3-4.2,11.4-4.1,15.8,0.2l30.6,29.9c4.4,4.3,4.5,11.3,0.2,15.5L264.1,380.9c-2.2,2.2-5.2,3.2-8.1,3c-3,0.1-5.9-0.9-8.1-3L35.2,176.7c-4.3-4.2-4.2-11.2,0.2-15.5L66,131.3c4.4-4.3,11.5-4.4,15.8-0.2L256,298.3z"
                    />
                </svg>
                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-4">
                    <path d="M417.4,224H288V94.6c0-16.9-14.3-30.6-32-30.6c-17.7,0-32,13.7-32,30.6V224H94.6C77.7,224,64,238.3,64,256 c0,17.7,13.7,32,30.6,32H224v129.4c0,16.9,14.3,30.6,32,30.6c17.7,0,32-13.7,32-30.6V288h129.4c16.9,0,30.6-14.3,30.6-32 C448,238.3,434.3,224,417.4,224z"></path>
                </svg>
            </span>
            <span class="zpaccordionicon zpaccord-icon-active">
                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-1">
                    <path d="M413.1,327.3l-1.8-2.1l-136-156.5c-4.6-5.3-11.5-8.6-19.2-8.6c-7.7,0-14.6,3.4-19.2,8.6L101,324.9l-2.3,2.6 C97,330,96,333,96,336.2c0,8.7,7.4,15.8,16.6,15.8v0h286.8v0c9.2,0,16.6-7.1,16.6-15.8C416,332.9,414.9,329.8,413.1,327.3z"></path>
                </svg>
                <svg viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-2">
                    <path d="M184.746,156.373c-1.639,0-3.275-0.625-4.525-1.875L128,102.278l-52.223,52.22c-2.497,2.5-6.55,2.5-9.05,0 c-2.5-2.498-2.5-6.551,0-9.05l56.749-56.747c1.2-1.2,2.828-1.875,4.525-1.875l0,0c1.697,0,3.325,0.675,4.525,1.875l56.745,56.747 c2.5,2.499,2.5,6.552,0,9.05C188.021,155.748,186.383,156.373,184.746,156.373z M256,128C256,57.42,198.58,0,128,0 C57.42,0,0,57.42,0,128c0,70.58,57.42,128,128,128C198.58,256,256,198.58,256,128z M243.2,128c0,63.521-51.679,115.2-115.2,115.2 c-63.522,0-115.2-51.679-115.2-115.2C12.8,64.478,64.478,12.8,128,12.8C191.521,12.8,243.2,64.478,243.2,128z"></path>
                </svg>
                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-3">
                    <path d="M256,213.7L256,213.7L256,213.7l174.2,167.2c4.3,4.2,11.4,4.1,15.8-0.2l30.6-29.9c4.4-4.3,4.5-11.3,0.2-15.5L264.1,131.1c-2.2-2.2-5.2-3.2-8.1-3c-3-0.1-5.9,0.9-8.1,3L35.2,335.3c-4.3,4.2-4.2,11.2,0.2,15.5L66,380.7c4.4,4.3,11.5,4.4,15.8,0.2L256,213.7z"
                    />
                </svg>
                <svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg" class="svg-icon-15px zpaccord-svg-icon-4">
                    <path d="M417.4,224H94.6C77.7,224,64,238.3,64,256c0,17.7,13.7,32,30.6,32h322.8c16.9,0,30.6-14.3,30.6-32 C448,238.3,434.3,224,417.4,224z"></path>
                </svg>
            </span>
        </div>
        <div data-element-id="elm_aPCWrLzWSj71lfwF5_Zi9A" data-element-type="accordioncontainer" class="zpelement zpaccordion-content "
            style="margin-top:0;">
            <div class="zpaccordion-element-container"></div>
        </div>
    </div>
</div>
```

## Containers

### <a name="box"></a>Box

A box is generated as a single `div` element.

```html
<div data-element-id="elm_Jk3qtdnJnd1q3F979w1faQ" data-element-type="box" class="zpelem-box zpelement zpbox-container zpdefault-section zpdefault-section-bg "
    data-background-type="none"></div>
```


### <a name="row"></a>Row

A row is an element with a Bootstrap-like 12 column setup. With a single column (the default), you get this HTML:

```html
<div data-element-id="elm_FhTJqP3qF1yxByslmd2ISA" data-element-type="row" class="zprow zpalign-items-flex-start zpjustify-content-flex-start"
    data-column-ratio="12,12">
</div>
```

If you set a ratio property to have multiple columns, the column `div`s will
be generated too. Below is the html of a row with the ratio set to 3-6-3.

```html
<div data-element-id="elm_FhTJqP3qF1yxByslmd2ISA" data-element-type="row" class="zprow zpalign-items-flex-start zpjustify-content-flex-start"
    data-column-ratio="3:6:3,12:12:12">
    <div data-element-id="elm_hqHBT7cs7LtcVsX-p2tg6A" data-element-type="column" class="zpcol-md-3 zpcol-sm-12 zpdefault-section zpdefault-section-bg "
        data-background-type="none"></div>
    <div data-element-id="elm_b0YO7MaIZPPpMGM4XEojcg" data-element-type="column" class="zpcol-md-6 zpcol-sm-12 zpdefault-section zpdefault-section-bg "
        data-background-type="none"></div>
    <div data-element-id="elm_tOdDBSUSgyo8UnW5Kaue1A" data-element-type="column" class="zpcol-md-3 zpcol-sm-12 zpdefault-section zpdefault-section-bg "
        data-background-type="none"></div>
</div>
```

## Dividers

### <a name="divider"></a>Divider

```html
<div data-element-id="elm_JfFwlG5DwBypxgRqjqHkSg" data-element-type="divider" class="zpelement zpelem-divider ">
    <style>
        [data-element-id="elm_JfFwlG5DwBypxgRqjqHkSg"] .zpdivider-container .zpdivider-common:after,
        [data-element-id="elm_JfFwlG5DwBypxgRqjqHkSg"] .zpdivider-container .zpdivider-common:before {
            border-color:
        }
    </style>
    <div class="zpdivider-container zpdivider-line zpdivider-align-center zpdivider-width100 zpdivider-line-style-solid "
        data-divider-border-color=>
        <div class="zpdivider-common"></div>
    </div>
</div>
```

### <a name="divider_icon"></a>Divider with Icon

```html
<div data-element-id="elm_ATr5nYrH40OdItB1uhbIfA" data-element-type="dividerIcon" class="zpelement zpelem-dividericon ">
    <style>
        [data-element-id="elm_ATr5nYrH40OdItB1uhbIfA"] .zpdivider-container .zpdivider-common:after,
        [data-element-id="elm_ATr5nYrH40OdItB1uhbIfA"] .zpdivider-container .zpdivider-common:before {
            border-color:  !important;
        }

        [data-element-id="elm_ATr5nYrH40OdItB1uhbIfA"] .zpdivider-container.zpdivider-icon .zpdivider-common svg {
            fill:  !important;
        }

        [data-element-id="elm_ATr5nYrH40OdItB1uhbIfA"] .zpdivider-container.zpdivider-style-border .zpdivider-common,
        [data-element-id="elm_ATr5nYrH40OdItB1uhbIfA"] .zpdivider-container.zpdivider-style-roundcorner .zpdivider-common,
        [data-element-id="elm_ATr5nYrH40OdItB1uhbIfA"] .zpdivider-container.zpdivider-style-circle .zpdivider-common {
            border-color:  !important;
        }

        [data-element-id="elm_ATr5nYrH40OdItB1uhbIfA"] .zpdivider-container.zpdivider-style-bgfill .zpdivider-common,
        [data-element-id="elm_ATr5nYrH40OdItB1uhbIfA"] .zpdivider-container.zpdivider-style-roundcorner-fill .zpdivider-common,
        [data-element-id="elm_ATr5nYrH40OdItB1uhbIfA"] .zpdivider-container.zpdivider-style-circle-fill .zpdivider-common {
            background:  !important;
        }
    </style>
    <div class="zpdivider-container zpdivider-icon zpdivider-align-center zpdivider-width100 zpdivider-line-style-solid zpdivider-icon-size-md zpdivider-style-none "
        data-divider-border-color="" data-divider-icon-color="" data-divider-icon-bg-color="" data-divider-icon-border-color="">
        <div class="zpdivider-common">
            <svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg">
                <path d="M1472 992v480q0 26-19 45t-45 19h-384v-384h-256v384h-384q-26 0-45-19t-19-45v-480q0-1 .5-3t.5-3l575-474 575 474q1 2 1 6zm223-69l-62 74q-8 9-21 11h-3q-13 0-21-7l-692-577-692 577q-12 8-24 7-13-2-21-11l-62-74q-8-10-7-23.5t11-21.5l719-599q32-26 76-26t76 26l244 204v-195q0-14 9-23t23-9h192q14 0 23 9t9 23v408l219 182q10 8 11 21.5t-7 23.5z"
                />
            </svg>
        </div>
    </div>
</div>
```

### <a name="divider_text"></a>Divider with Text

```html
<div data-element-id="elm_SM8Pss0rQu9vXVAyNbRgHw" data-element-type="dividerText" class="zpelement zpelem-dividertext ">
    <style>
        [data-element-id="elm_SM8Pss0rQu9vXVAyNbRgHw"] .zpdivider-container .zpdivider-common:after,
        [data-element-id="elm_SM8Pss0rQu9vXVAyNbRgHw"] .zpdivider-container .zpdivider-common:before {
            border-color:  !important;
        }

        [data-element-id="elm_SM8Pss0rQu9vXVAyNbRgHw"] .zpdivider-container.zpdivider-text .zpdivider-common {
            color:  !important;
        }

        [data-element-id="elm_SM8Pss0rQu9vXVAyNbRgHw"] .zpdivider-container.zpdivider-style-border .zpdivider-common,
        [data-element-id="elm_SM8Pss0rQu9vXVAyNbRgHw"] .zpdivider-container.zpdivider-style-roundcorner .zpdivider-common,
        [data-element-id="elm_SM8Pss0rQu9vXVAyNbRgHw"] .zpdivider-container.zpdivider-style-circle .zpdivider-common {
            border-color:  !important;
        }

        [data-element-id="elm_SM8Pss0rQu9vXVAyNbRgHw"] .zpdivider-container.zpdivider-style-bgfill .zpdivider-common,
        [data-element-id="elm_SM8Pss0rQu9vXVAyNbRgHw"] .zpdivider-container.zpdivider-style-roundcorner-fill .zpdivider-common,
        [data-element-id="elm_SM8Pss0rQu9vXVAyNbRgHw"] .zpdivider-container.zpdivider-style-circle-fill .zpdivider-common {
            background:  !important;
        }
    </style>
    <div class="zpdivider-container zpdivider-text zpdivider-align-center zpdivider-width100 zpdivider-line-style-solid zpdivider-style-none "
        data-divider-border-color="" data-divider-text-color="" data-divider-text-bg-color="" data-divider-text-border-color="">
        <div class="zpdivider-common">Sample Text Goes Here</div>
    </div>
</div>
```

### <a name="spacer"></a>Spacer

```html
<div data-element-id="elm_KHUFhdV1T4vrFCZHhOHW-Q" data-element-type="spacer" class="zpelement zpelem-spacer">
    <style>
        div[data-element-id="elm_KHUFhdV1T4vrFCZHhOHW-Q"] div.zpspacer {
            height: 30px;
        }

        @media (max-width: 768px) {
            div[data-element-id="elm_KHUFhdV1T4vrFCZHhOHW-Q"] div.zpspacer {
                height: calc(30px / 3);
            }
        }
    </style>
    <div class="zpspacer " data-height="30"></div>
</div>
```
