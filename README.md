# Bootstrap 4 StarterKit for Handlebars

This StarterKit for is a Bootstrap 4 Starterkit for Handlebars is meant to be used as a starting point for Bootstrap 4 Handlebars-based  Pattern Lab projects.

## Requirements

The Bootstrap 4 StarterKit for Handlebars requires the following PatternEngine:

- `@pattern-lab/patternengine-node-handebars`: [npm](https://www.npmjs.com/package/@pattern-lab/patternengine-node-handebars), [Github](https://github.com/pattern-lab/patternengine-node-handlebars)

## Install

[Installation Instructions](http://patternlab.io/docs/advanced-starterkits.html)

## Edit Files

After installation the files for this StarterKit can be found in `source/`.