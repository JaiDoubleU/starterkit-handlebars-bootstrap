# How to improve this setup?

Here are the steps to improve this setup:

* Check the open Merge Requests to see the pending contributions.
* Pick an undocumented item from the list below.
* Install [the minimal template](docs/minimal.md) provided in this repository
* For the item you want to document, add it as an element to your test site
* Add a template file to the folder `source/_patterns` replicating the generated HTML and CSS classes.
* Submit a Gitlab Merge Request

It is possible that the [page template](minimal/page.face) must be extended to display
more of the generated content.
Possible improvements:

* Sections
   * Heading
   * Image
   * Icon
   * Newsletter
   * Box
   * Tab
   * Accordion
   * Carousel
* Forms
   * Contact Us (Other Forms)
* Visual Editor Support
